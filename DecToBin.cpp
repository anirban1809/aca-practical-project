#include<iostream>
#include<string>
using namespace std;

string convert(int dec){
	int r=dec;
	int x;
	string out="";
	while(r!=0){
		x=r%2;
		out=(char)(x+48)+out;
		r/=2;
	}
	return out;
}

int main(){
	cout<<"Enter number in decimal:";
	int d;
	cin>>d;
	cout<<"\n"<<d<<" in binary:"<<convert(d)<<endl;
}


