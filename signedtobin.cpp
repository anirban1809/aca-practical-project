#include<iostream>
#include<string>
using namespace std;

string convert(int dec){
	int r=dec;
	int x;
	string out="";
	int flag=0;
	while(r!=0){
		x=r%2;
		if(flag==0){
			out=(char)(x+48)+out;
			if(x==1){
				flag=1;
				continue;
			}
		}else{
			if(x==1){
				out='0'+out;	
			}else{
				out='1'+out;
			}
		}
		r/=2;
	}
	if(dec<0){
		out="1"+out;
		int i;
		for(i=0;i<out.length();i++){
			if(out.at(i)=='/'){
				out.at(i)='0';
			}
		}
	}
	return out;
}

int main(){
	cout<<"Enter number in decimal:";
	int d;
	cin>>d;
	cout<<"Signed binary of "<<d<<" : "<<convert(d)<<endl;
}


