import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class Application{
	int input[];
	int weights[][];

	public void input()throws IOException{
		BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
		input=new int[2];
		weights = new int[2][2];

		for(int i=0;i<2;i++){
			System.out.print("Enter input value:");
			input[i] = Integer.parseInt(read.readLine());
		}

		for(int i=0;i<2;i++){
			System.out.println("Input "+i+":");
			for(int j=0;j<2;j++){
				System.out.print("Enter weight "+j+":");
				weights[i][j] = Integer.parseInt(read.readLine());
			}
		}
	}
	public static void main(String args[])throws IOException{
		Application app = new Application();
		app.input();
		ThreadDemo thread = new ThreadDemo(app.input,app.weights);
		thread.start();

	}
}